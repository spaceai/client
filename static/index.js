(function () {
    const params = {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    }
    fetch('/meds', params).then( done => done.json() )
    .then( data => {
        $('select[name="s"]').append(
            data.map( m => (`<option value="${m.value}">${m.label}</option>`))
        )
    });

    // setup socket
    const namespace = '/test';
    const socket = io.connect(location.protocol +'//'+ document.domain +':'+ location.port + namespace);

    // receive
    socket.on('my_response', function(msg) {
        console.log(msg)
        if (msg.data=='Connected') {
            ping_pong_times = []
        } 
        if (msg.queue) {
            const li = msg.queue.reduce((r,c) => (`${r}<li class="list-group-item">${c}</li>`), '')
            $('#queue > .list-group').html(li)
        }
    })

    // send
    $('form#emit').submit(function(event) {
        socket.emit('my_event', { data: $('#emit').serialize() });  //  message['data']
        $('#emit').trigger('reset');
        return false;
    });

    // Interval function that tests message latency by sending a "ping" message.
    var ping_pong_times = [];
    var start_time;
    window.setInterval(function() {
        start_time = (new Date).getTime();
        socket.emit('my_ping');
    }, 10000);

    // Handler for the "pong" message.
    socket.on('my_pong', function(msg) {
        console.log(msg);
        var latency = (new Date).getTime() - start_time;
        ping_pong_times.push(latency);
        ping_pong_times = ping_pong_times.slice(-30); // keep last 30 samples
        var sum = 0;
        for (var i = 0; i < ping_pong_times.length; i++)
            sum += ping_pong_times[i];
        $('#ping-pong').text(Math.round(10 * sum / ping_pong_times.length) / 10);
    });
})();
