import datetime
import time
from os import listdir, remove

import serial
import threading

from flask import Flask, render_template, request, Response
from flask_socketio import SocketIO, emit, disconnect

#####################################################

app = Flask(__name__)
app.config['SECRET_KEY'] = 'H3ll0 s0ck3t!'

async_mode = None
socketio = SocketIO(app, async_mode=async_mode)

s = serial.Serial('/dev/ttyUSB0', 19200, timeout=5)

#####################################################

spinner = '-,\,|,/'.split(',')
spinpos = 0

mqx = []
pop = -1
service_available = False


def handle_data(data):
    global pop
    global spinner, spinpos
    if(data > ''):
        if (data.startswith('+SBDIX: 0,') or data.startswith('+SBDIX: 1,') or data.startswith('+SBDIX: 2,')):
            for x in mqx[0:pop+1]:
                remove(x['file'])

            del mqx[0:pop+1]
            pop = -1
            # update queue in client?
            socketio.emit('my_response', { 'data': data, 'queue': mqx }, namespace='/test')
            print('Popping from queue: '+ data)

        print(data)
    else:
        print('1,2,3,4'.split(',')[spinpos], end='\b')
        spinpos =(spinpos + 1)% 4
    
    if data =='+CIEV:1,1' and len(mqx):
        write_and_send()


def write_and_send():
    # write to buffer and try to send...
    pop = -1
    sum = 0
    for itm in mqx:
        if sum + len(itm['data']) + 1 > 120:
            break
        pop += 1
        sum += len(itm['data']) + 1
    
    t = ','.join(list(map(lambda m: m['data'], mqx[0:pop+1])))

    dummy = write_to_port('+SBDWT='+ t) # 120 chars max
    lines = write_to_port('+SBDIX') # if successful then pop(0)
    print(lines)


def write_to_port(command):
    strCmd = "AT"+ command
    bytCmd = bytes(strCmd +"\r", 'utf-8')
    s.write(bytCmd)

    line = s.readline().decode().strip()
    lines = [line]
    while (line!='OK' and line!='ERROR'):
        # handle_data(line)
        line = s.readline().decode().strip()
        lines.append(line)

    # handle_data(line)
    return lines


def read_from_port(s):
    # init
    # preload queue with cold storage
    for file in listdir():
        if file.endswith('.que'):
            fh = open(file, 'r')
            data = fh.read()
            fh.close()

            mqx.append({"file": file, "data": data})
    
    # heartbeat
    global spinner, spinpos
    write_to_port('+GSN')
    write_to_port('+CIER=1,0,1,0')
    #
    # main loop
    #
    while s.is_open:
        if len(mqx):
            print('[over]write to buffer... returns OK | ERROR')
            write_and_send()

        if (s.in_waiting > 0):
            line = s.readline().decode().strip()
            handle_data(line)
        else:
            print(spinner[spinpos], end='\b')
            spinpos =(spinpos + 1)% 4
            time.sleep(20)
    
    print('Serial closed')


thread_serial = threading.Thread(target=read_from_port, args=(s,))
thread_serial.start()

###################################################################

@app.route('/')
def index():
    return render_template('index.html', async_mode=socketio.async_mode)

@app.route('/meds')
def json_meds():
    with app.open_resource('static/meds.json') as file:
        my_json = file.read().decode('utf-8')
        return Response(my_json, mimetype='application/json')

@socketio.on('connect', namespace='/test')
def test_connect():
    socketio.emit('my_response', { 'data': 'Connected', 'queue': mqx }, namespace='/test')
    print('Socket connected, queue: '+ '|'.join(list(map(lambda m: m['data'], mqx))) )

@socketio.on('my_event', namespace='/test')
def add_to_queue(message):
    file = '{:%Y%m%d_%H%M%S}.que'.format(datetime.datetime.now())
    data = message['data']

    mqx.append({"file": file, "data": data})
    
    fh = open(file, 'w+')
    fh.write(data)
    fh.close()

    socketio.emit('my_response', { 'data': 'Added to queue', 'queue': mqx }, namespace='/test')
    print('Added to queue: '+ ','.join(list(map(lambda m: m['data'], mqx))) )

@socketio.on('my_ping', namespace='/test')
def ping_pong():
    socketio.emit('my_pong', { 'data': 'Ping-pong', 'queue': mqx }, namespace='/test')
